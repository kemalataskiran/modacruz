package com.modacruz.exception.handler;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.modacruz.api.ProductController;
import com.modacruz.exception.ProductAlreadyExistException;
import com.modacruz.exception.ResourceNotFoundException;
import com.modacruz.exception.details.ValidationErrorDetails;

@ControllerAdvice
public class RestExceptionHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	@Autowired
    private MessageSource messageSource;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorDetails processValidationError(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();

        return processFieldErrors(fieldErrors);
    }

    private ValidationErrorDetails processFieldErrors(List<FieldError> fieldErrors) {
        ValidationErrorDetails validationErrorDetails = new ValidationErrorDetails();

        for (FieldError fieldError: fieldErrors) {
            String localizedErrorMessage = resolveLocalizedErrorMessage(fieldError);
            validationErrorDetails.addFieldError(fieldError.getField(), localizedErrorMessage);
        }

        return validationErrorDetails;
    }

    private String resolveLocalizedErrorMessage(FieldError fieldError) {
        Locale currentLocale =  LocaleContextHolder.getLocale();
        String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);

        //If the message was not found, return the most accurate field error code instead.
        //You can remove this check if you prefer to get the default error message.
        if (localizedErrorMessage.equals(fieldError.getDefaultMessage())) {
            String[] fieldErrorCodes = fieldError.getCodes();
            localizedErrorMessage = fieldErrorCodes[0];
        }

        return localizedErrorMessage;
    }
    
	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletRequest request, ResourceNotFoundException ex) {
		LOGGER.debug("handling 404 error on a entry" + request.getContextPath());
	}

	@ExceptionHandler(ProductAlreadyExistException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public void handleBuildingAlreadyExistException(HttpServletRequest request, ProductAlreadyExistException ex) {
		LOGGER.debug("handling 404 error on a entry" + request.getContextPath());
	}
	

}
