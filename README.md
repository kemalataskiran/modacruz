# product-service

Product service is demonstrating product add and get operations.
Product is entered the sytems and inserted to database. Product details are taken with id. 

Visually, rest api methods can be seen in [http://localhost:8000/swagger-ui.html]. Swagger is used for this issue.

## What's inside 
This project is based on the [Spring Boot](http://projects.spring.io/spring-boot/) project and uses these packages :
-Maven 
-Spring Boot
-Spring Data (Hibernate & H2 database & MongoDB)
-QueryDsl
-Lombok
-Model Wrapper
-Swagger
-Unit Test, Mockito, Hamcrest, Json Path, AssertJ

## Installation 
The project is created with Maven, so you just need to import it to your IDE and build the project to resolve the dependencies.
It uses embedded Postgres database for relational database, Redis for nosql database, 
in resources/application.yml, values in the below must be set according to database instance.

redis:
 host: localhost
 port: 6379
	
datasource:
 url: jdbc:postgresql://localhost:5432/weasel
 username: postgres
 password: 123456

## Usage 
Run the project and use the services 

Rest services explained detailly in [http://localhost:8000/swagger-ui.html]

