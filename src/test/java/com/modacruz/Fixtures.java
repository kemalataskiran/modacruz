package com.modacruz;


import com.modacruz.domain.Product;
import com.modacruz.domain.enumaration.Color;
import com.modacruz.domain.enumaration.Size;
import com.modacruz.model.ProductDetails;
import com.modacruz.model.ProductForm;
import com.modacruz.util.DTOUtils;

public class Fixtures {

	public static Product createProduct(String name){
		return createProduct(null, name, null);
	}

	public static Product createProduct(String name, String description){
		return createProduct(null, name, description);
	}

	public static Product createProduct(Long id, String name){
		return createProduct(id, name, null);
	}

	public static Product createProduct(Long id, String name, String description) {
		Product product = Product.builder()
				.id(id)
				.name(name)
				.description(description)
				.color(Color.BLUE)
				.price(5.0)
				.size(Size.M)
				.build();
		return product;

	}


	public static ProductDetails createProductDetails(Long id, String name, String description) {
		Product product = createProduct(id, name,description);
		return DTOUtils.map(product, ProductDetails.class);
	}
	public static ProductDetails createProductDetails(String name, String description) {
		return createProductDetails(null, name, description);
	}
	public static ProductDetails createProductDetails(Long id , String name) {
		Product product = createProduct(id, name,null);
		return DTOUtils.map(product, ProductDetails.class);
	}
	
	public static ProductForm createProductForm(String name, String description){
		return createProductForm(null, name, description);
	}
	
	public static ProductForm createProductForm(Long id, String name, String description) {
		Product product = createProduct(id, name,description);
		return DTOUtils.map(product, ProductForm.class);
	}
	

}
