package com.modacruz;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Petri Kainulainen
 */
public class TestUtil1 {

    private static final String DEFAULT_CHARACTER = "a";

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    public static String createStringWithLength(int length) {
        StringBuilder builder = new StringBuilder();

        for (int index = 0; index < length; index++) {
            builder.append(DEFAULT_CHARACTER);
        }

        return builder.toString();
    }

    public static String createStringWithLength(int length, char character) {
    	StringBuilder builder = new StringBuilder();
    	
    	for (int index = 0; index < length; index++) {
    		builder.append(character);
    	}
    	
    	return builder.toString();
    }
}
