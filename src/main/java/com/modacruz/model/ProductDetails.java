package com.modacruz.model;

import java.io.Serializable;

import com.modacruz.domain.enumaration.Color;
import com.modacruz.domain.enumaration.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDetails implements Serializable {
	
	private static final long serialVersionUID = -611682099574764987L;

    private Long id;
	
	private String name;

	private String description;
	
	private Color color;

	private double price;

	private Size size;

}
