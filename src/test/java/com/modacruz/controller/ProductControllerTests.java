package com.modacruz.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.modacruz.Fixtures;
import com.modacruz.TestUtil1;
import com.modacruz.api.ProductController;
import com.modacruz.domain.Product;
import com.modacruz.exception.ProductAlreadyExistException;
import com.modacruz.exception.ResourceNotFoundException;
import com.modacruz.model.ProductDetails;
import com.modacruz.model.ProductForm;
import com.modacruz.service.ProductService;
import com.modacruz.util.Constants;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ProductController.class, secure = false)
public class ProductControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ProductService service;
	
//================================================CREATE=====================================================================
	@Test
    public void testCreate_NullProductForm_ShouldThrowException() throws Exception{
        byte[] bytes = null;
        
        this.mockMvc.perform(post(Constants.URI_PRODUCT)
        		.accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(bytes)
        )
                .andExpect(status().isBadRequest());
 
        verifyZeroInteractions(service);
	}
	
	@Test
	public void testCreate_AlreadyExistProduct_ShouldThrowException() throws Exception {
		ProductForm productForm = Fixtures.createProductForm("Product1", "Description1");
        when(service.create(any(ProductForm.class))).thenThrow(new ProductAlreadyExistException(productForm.getName()));
        
        this.mockMvc.perform(post(Constants.URI_PRODUCT)
        		.accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestUtil1.convertObjectToJsonBytes(productForm))
        )
                .andExpect(status().isBadRequest());

        ArgumentCaptor<ProductForm> formCaptor = ArgumentCaptor.forClass(ProductForm.class);
        verify(service, times(1)).create(formCaptor.capture());
        verifyNoMoreInteractions(service);
 
        ProductForm formArgument = formCaptor.getValue();
        assertThat(formArgument.getName(), is(productForm.getName()));
        assertThat(formArgument.getDescription(), is(productForm.getDescription()));
	}
	
  	@Test
  	public void testCreate_ValidationNotNull_NameAndDescription_ShouldThrowException() throws Exception {
        ProductForm productForm = Fixtures.createProductForm(null,null);
        productForm.setName(null);
        productForm.setDescription(null);
 
        this.mockMvc.perform(post(Constants.URI_PRODUCT)
        		.accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestUtil1.convertObjectToJsonBytes(productForm))
        )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.fieldErrors", hasSize(2)))
                .andExpect(jsonPath("$.fieldErrors[*].field", containsInAnyOrder("name", "description")))
                .andExpect(jsonPath("$.fieldErrors[*].message", containsInAnyOrder(
                 "The name of the product cannot be empty.",
                 "The description of the product cannot be empty."
                ))).andDo(print());
 
        verifyZeroInteractions(service);
  	}
  	  	
	@Test
    public void testCreate_ValidationSize_NameAndDescription_ShouldThrowException() throws Exception {
        String name = TestUtil1.createStringWithLength(Product.MAX_LENGTH_NAME+1);
        String description = TestUtil1.createStringWithLength(Product.MAX_LENGTH_DESCRIPTION+1);
        ProductForm productForm = Fixtures.createProductForm(name, description);
 
        this.mockMvc.perform(post(Constants.URI_PRODUCT)
        		.accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestUtil1.convertObjectToJsonBytes(productForm))
        )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.fieldErrors", hasSize(2)))
                .andExpect(jsonPath("$.fieldErrors[*].field", containsInAnyOrder("name", "description")))
                .andExpect(jsonPath("$.fieldErrors[*].message", containsInAnyOrder(
                 "The maximum length of the description is 500 characters.",
                 "The maximum length of the name is 100 characters."
                ))).andDo(print());
 
        verifyZeroInteractions(service);
    }
	
    @Test
    public void testCreate_NewProduct_ShouldSaveEntry() throws Exception{
    	
    	ProductForm productForm = Fixtures.createProductForm("Product1", "Description1");
    	ProductDetails productDetails=Fixtures.createProductDetails("Product1", "Description1");
        when(service.create(any(ProductForm.class))).thenReturn(productDetails);
 
        this.mockMvc.perform(post(Constants.URI_PRODUCT)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestUtil1.convertObjectToJsonBytes(productForm))
        )
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name", is(productDetails.getName())))
                .andExpect(jsonPath("$.description", is(productDetails.getDescription())));
 
        ArgumentCaptor<ProductForm> formCaptor = ArgumentCaptor.forClass(ProductForm.class);
        verify(service, times(1)).create(formCaptor.capture());
        verifyNoMoreInteractions(service);
 
        ProductForm formArgument = formCaptor.getValue();
        assertThat(formArgument.getName(), is(productForm.getName()));
        assertThat(formArgument.getDescription(), is(productForm.getDescription()));
    }


//================================================UPDATE =====================================================================
	@Test
    public void testUpdate_NullProductForm_ShouldThrowException() throws Exception{
        byte[] bytes = null;
        
        this.mockMvc.perform(put(Constants.URI_PRODUCT + "/{id}", 1L)
        		.accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(bytes)
        )
                .andExpect(status().isBadRequest());
 
        verifyZeroInteractions(service);
	}
	
	@Test
	public void testUpdate_NotExistEntry_ShouldThrowException() throws Exception {
	   	ProductForm productForm = Fixtures.createProductForm("Product1", "desc1");

        when(service.update(anyLong(),any(ProductForm.class))).thenThrow(new ResourceNotFoundException(""));

        this.mockMvc.perform(put(Constants.URI_PRODUCT + "/{id}", 3L)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestUtil1.convertObjectToJsonBytes(productForm))
        )
                .andExpect(status().isNotFound());

        ArgumentCaptor<ProductForm> formCaptor = ArgumentCaptor.forClass(ProductForm.class);
        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);
        verify(service, times(1)).update(idCaptor.capture(),formCaptor.capture());
        verifyNoMoreInteractions(service);

        ProductForm formArgument = formCaptor.getValue();
        assertThat(idCaptor.getValue().longValue(), is(3L));
        assertThat(formArgument.getName(), is(productForm.getName()));
        assertThat(formArgument.getDescription(), is(productForm.getDescription()));
	}


  	@Test
  	public void testUpdate_ValidationNotNull_NameAndDescription_ShouldThrowException() throws Exception {
        ProductForm productForm = Fixtures.createProductForm(null,null);
        productForm.setName(null);
        productForm.setDescription(null);
 
        this.mockMvc.perform(put(Constants.URI_PRODUCT + "/{id}", 1L)
        		.accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestUtil1.convertObjectToJsonBytes(productForm))
        )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.fieldErrors", hasSize(2)))
                .andExpect(jsonPath("$.fieldErrors[*].field", containsInAnyOrder("name", "description")))
                .andExpect(jsonPath("$.fieldErrors[*].message", containsInAnyOrder(
                 "The name of the product cannot be empty.",
                 "The description of the product cannot be empty."
                ))).andDo(print());
 
        verifyZeroInteractions(service);
  	}
  	  	
	@Test
    public void testUpdate_ValidationSize_NameAndDescription_ShouldThrowException() throws Exception {
        String name = TestUtil1.createStringWithLength(Product.MAX_LENGTH_NAME+1);
        String description = TestUtil1.createStringWithLength(Product.MAX_LENGTH_DESCRIPTION+1);
        ProductForm productForm = Fixtures.createProductForm(name, description);
 
        this.mockMvc.perform(put(Constants.URI_PRODUCT + "/{id}", 1L)
        		.accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestUtil1.convertObjectToJsonBytes(productForm))
        )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.fieldErrors", hasSize(2)))
                .andExpect(jsonPath("$.fieldErrors[*].field", containsInAnyOrder("name", "description")))
                .andExpect(jsonPath("$.fieldErrors[*].message", containsInAnyOrder(
                 "The maximum length of the description is 500 characters.",
                 "The maximum length of the name is 100 characters."
                ))).andDo(print());
 
        verifyZeroInteractions(service);
    }

    
    @Test
    public void testUpdate_ExistingEntry_ShouldUpdateEntry() throws Exception {
    	ProductForm productForm = Fixtures.createProductForm("Product1", "desc1");
    	ProductDetails productDetails = Fixtures.createProductDetails("Product1", "desc1");
    	
        when(service.update(anyLong(),any(ProductForm.class))).thenReturn(productDetails);

        this.mockMvc.perform(put(Constants.URI_PRODUCT + "/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestUtil1.convertObjectToJsonBytes(productForm))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.description", is(productDetails.getDescription())))
                .andExpect(jsonPath("$.name", is(productDetails.getName())));

        ArgumentCaptor<ProductForm> formCaptor = ArgumentCaptor.forClass(ProductForm.class);
        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);
        verify(service, times(1)).update(idCaptor.capture(),formCaptor.capture());
        verifyNoMoreInteractions(service);

        ProductForm formArgument = formCaptor.getValue();
        assertThat(idCaptor.getValue().longValue(), is(1L));
        assertThat(formArgument.getDescription(), is(productForm.getDescription()));
        assertThat(formArgument.getName(), is(productForm.getName()));
    }

//================================================DELETE=====================================================================
    @Test
    public void testDelete_NotExistId_ShouldThrowException_HttpStatusCode404() throws Exception {
    	doNothing().when(service).delete(anyLong());
    	
        this.mockMvc.perform(delete(Constants.URI_PRODUCT + "/{id}", 3L))
                .andExpect(status().isNoContent());

        verify(service, times(1)).delete(3L);
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testDelete_ExistingId_ShouldDeleteEntry_HttpStatusCode204() throws Exception {
        
    	doNothing().when(service).delete(anyLong());
        this.mockMvc.perform(delete(Constants.URI_PRODUCT + "/{id}", 1L))
                .andExpect(status().isNoContent());

        verify(service, times(1)).delete(1L);
        verifyNoMoreInteractions(service);
    
    }
    
//================================================FIND BY ID=====================================================================

  	@Test
  	public void testFindById_NotExistEntry_ShouldThrowException_HttpStatusCode404() throws Exception {
  		assertThat(this.service).isNotNull();
  		when(service.findById(1L)).thenThrow(new ResourceNotFoundException(""));

  		this.mockMvc.perform(get(Constants.URI_PRODUCT + "/{id}", 1L))
  				.andExpect(status().isNotFound());

  		verify(service, times(1)).findById(1L);
  		verifyNoMoreInteractions(service);
  	}

    @Test
  	public void testFindById_ExistingEntry_ShouldReturnFoundEntry() throws Exception {
  		assertThat(this.service).isNotNull();
  		ProductDetails productDetails = Fixtures.createProductDetails(1L, "Product1");
  		when(this.service.findById(anyLong())).thenReturn(productDetails);
  		
  		this.mockMvc.perform(get(Constants.URI_PRODUCT + "/{id}", 1L))
  				.andExpect(status().isOk())
  				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
  				.andExpect(jsonPath("$.id", is(1)))
  	            .andExpect(jsonPath("$.name", is("Product1")));

  		verify(service, times(1)).findById(anyLong());
  		verifyNoMoreInteractions(service);
  	}

  	
  	

//================================================FIND ALL=====================================================================
  	@Test
      public void testFindAll_ShouldReturnListOfEntries() throws Exception{
  		assertThat(this.service).isNotNull();
  		ProductDetails productDetails1 = Fixtures.createProductDetails(1L, "Product1");
  		ProductDetails productDetails2 = Fixtures.createProductDetails(2L, "Product2");
  		when(this.service.findAll()).thenReturn(Arrays.asList(productDetails1, productDetails2));

  		this.mockMvc.perform(MockMvcRequestBuilders.get(Constants.URI_PRODUCT))
  				.andExpect(status().isOk())
  				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
  				.andExpect(jsonPath("$", hasSize(2)))
  				.andExpect(jsonPath("$[0].id", is(1)))
  				.andExpect(jsonPath("$[0].name", is("Product1")))
  				.andExpect(jsonPath("$[1].id", is(2)))
  				.andExpect(jsonPath("$[1].name", is("Product2")))
  				;

  		verify(service, times(1)).findAll();
  		verifyNoMoreInteractions(service);
  	}

}