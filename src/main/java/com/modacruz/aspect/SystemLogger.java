package com.modacruz.aspect;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SystemLogger {

  @Autowired
  private StringRedisTemplate template;

  private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  @Around("execution(* com.modacruz.api.ProductController..*(..))")
  public Object log(ProceedingJoinPoint pjp) throws Throwable {
    try {
      MethodSignature signature = (MethodSignature) pjp.getSignature();
      Method method = signature.getMethod();
      ValueOperations<String, String> ops = this.template.opsForValue();
      String key = LocalDateTime.now().format(formatter);
      ops.set(key, method + " is called");
    } catch (Exception e) {
      // do not anything
    }
    return pjp.proceed();
  }

}
