package com.modacruz.repository;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

import com.modacruz.Fixtures;
import com.modacruz.TestUtil1;
import com.modacruz.domain.Product;
import com.modacruz.exception.ProductAlreadyExistException;


@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository repository;

    
//================================================CREATE=====================================================================
  	@Test
    public void testCreate_NullProduct_ShouldThrowException() throws ProductAlreadyExistException{
  		Product model=null;
  		assertThatThrownBy(() -> repository.save(model)).isInstanceOf(InvalidDataAccessApiUsageException.class);
    }

  	@Test
  	public void testCreate_ValidationNotNull_Name_ShouldThrowException() throws ProductAlreadyExistException {
    	Product product = Fixtures.createProduct(null);
    	assertThatThrownBy(() -> this.repository.save(product)).isInstanceOf(ConstraintViolationException.class);
  	}
  	
  	@Test
  	public void testCreate_ValidationSize_Name_ShouldThrowException() throws ProductAlreadyExistException {
  		String name = TestUtil1.createStringWithLength(Product.MAX_LENGTH_NAME+1);
  		Product product = Fixtures.createProduct(name);
  		assertThatThrownBy(() -> this.repository.save(product)).isInstanceOf(ConstraintViolationException.class);
  	}

  	@Test
  	public void testCreate_ValidationSize_Description_ShouldThrowException() throws ProductAlreadyExistException {
  		String description = TestUtil1.createStringWithLength(Product.MAX_LENGTH_DESCRIPTION+1);
  		Product product = Fixtures.createProduct("Product1",description);
  		assertThatThrownBy(() -> this.repository.save(product)).isInstanceOf(ConstraintViolationException.class);
  	}
  	
   @Test
   public void testCreate_NewProduct_ShouldSaveEntry() throws ProductAlreadyExistException {
      	Product preDefinedProduct = Fixtures.createProduct("Product1","Description1");
      	Product persistedProduct = this.entityManager.persist(preDefinedProduct);
      	assertThat(preDefinedProduct.getName()).isEqualTo(persistedProduct.getName());
   }
    
//================================================DELETE=====================================================================
      
    @Test
  	public void testDelete_NullId_ShouldThrowException() {
  		Long id=null;
  		assertThatThrownBy(() -> repository.delete(id)).isInstanceOf(InvalidDataAccessApiUsageException.class);
  	}

  	@Test
  	public void testDelete_NotExistId_ShouldThrowException() {
  		assertThatThrownBy(() -> repository.delete(5L)).isInstanceOf(EmptyResultDataAccessException.class);
  	}

        
  	@Test
  	public void testDelete_ExistingId_ShouldDeleteEntry() {
    	Product preDefinedProduct = Fixtures.createProduct("Product1","Description1");
    	Product persistedProduct = this.entityManager.persist(preDefinedProduct);
    	
    	this.repository.delete(persistedProduct.getId());
        
    	Product product = this.repository.findOne(persistedProduct.getId());
        assertNull(product);
  	}



//================================================FIND BY ID=====================================================================
    @Test
    public void testFindById_NullId_ShouldThrowException() throws Exception {
    	Long myid=null;
    	assertThatThrownBy(() -> this.repository.findOne(myid)).isInstanceOf(InvalidDataAccessApiUsageException.class);
    }

  	@Test
    public void testFindById_NotExistEntry_ShouldThrowException() throws Exception {
        Product product = this.repository.findOne(1L);
        assertThat(product).isEqualTo(null);
    }

    @Test
    public void testFindById_ExistingEntry_ShouldReturnFoundEntry() throws Exception {
    	Product preDefinedProduct = Fixtures.createProduct("Product1","Description1");
    	Product persistedProduct = this.entityManager.persist(preDefinedProduct);
    	
        Product product = this.repository.findOne(persistedProduct.getId());
        assertThat(product).isEqualTo(persistedProduct);
        assertThat(product.getName()).isEqualTo(preDefinedProduct.getName());
    }

    

//================================================FIND ALL=====================================================================
    @Test
    public void testFindAll_ShouldReturnListOfEntries() throws Exception {
    	Product preDefinedProduct1 = Fixtures.createProduct("Product1","Description1");
    	Product persistedProduct1 = this.entityManager.persist(preDefinedProduct1);

    	Product preDefinedProduct2 = Fixtures.createProduct("Product2","Description1");
    	Product persistedProduct2 = this.entityManager.persist(preDefinedProduct2);
    	
    	List<Product> model = Arrays.asList(persistedProduct1,persistedProduct2);
        
    	List<Product> actual = this.repository.findAll();

        assertThat(actual.size()).isEqualTo(2);
        assertThat(actual).isEqualTo(model);
    }

}