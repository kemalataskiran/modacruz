package com.modacruz.model;

import java.io.Serializable;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.modacruz.domain.Product;
import com.modacruz.domain.enumaration.Color;
import com.modacruz.domain.enumaration.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductForm implements Serializable{

	private static final long serialVersionUID = -8158112759255939674L;

	@NotEmpty
	@Length(max = Product.MAX_LENGTH_NAME)
	private String name;
	
	@NotEmpty
	@Length(max = Product.MAX_LENGTH_DESCRIPTION)
	private String description;
	
	private Color color;

	private double price;

	private Size size;

	
}
