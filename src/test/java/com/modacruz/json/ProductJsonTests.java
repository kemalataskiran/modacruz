package com.modacruz.json;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

import com.modacruz.Fixtures;
import com.modacruz.domain.Product;
import com.modacruz.model.ProductDetails;
import com.modacruz.util.DTOUtils;

@RunWith(SpringRunner.class)
@JsonTest
public class ProductJsonTests {

    @Autowired
    private JacksonTester<ProductDetails> json;

    
    @Test
    public void testSerialize() throws Exception {
		Product product = Fixtures.createProduct(1L, "Product1","desc1");
		ProductDetails productDetails = DTOUtils.map(product, ProductDetails.class);

        assertThat(this.json.write(productDetails)).isEqualToJson("expected-product.json");
        assertThat(this.json.write(productDetails)).hasJsonPathStringValue("@.name");
        assertThat(this.json.write(productDetails)).extractingJsonPathStringValue("@.name")
                .isEqualTo("Product1");
    }

    @Test
    public void testDeserialize() throws Exception {
		Product product = Fixtures.createProduct(1L, "Product1","desc1");
		ProductDetails productDetail = DTOUtils.map(product, ProductDetails.class);

        String content = "{\"id\":1,\"name\":\"Product1\",\"price\":5.0,\"description\":\"desc1\",\"size\":\"M\",\"color\":\"BLUE\"}";
        assertThat(this.json.parse(content))
                .isEqualTo(productDetail);
        assertThat(this.json.parseObject(content).getName()).isEqualTo("Product1");
    }

}