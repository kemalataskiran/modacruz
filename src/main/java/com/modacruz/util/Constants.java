package com.modacruz.util;

public class Constants {

    /**
     * prefix of REST API
     */
    public static final String URI_API_PREFIX = "/api/modacruz-service";

    public static final String URI_PRODUCT = URI_API_PREFIX + "/product";

    private Constants() {
    }
    
}
