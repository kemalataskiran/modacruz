package com.modacruz.domain.enumaration;

public enum Color {
	RED,GREEN,BLUE,YELLOW,PINK,BLACK,WHITE,PURPLE,ORANGE,GREY;
}
