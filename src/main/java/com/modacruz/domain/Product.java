package com.modacruz.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.modacruz.domain.enumaration.Color;
import com.modacruz.domain.enumaration.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table
public class Product {
    public static final int MAX_LENGTH_DESCRIPTION = 500;
    public static final int MAX_LENGTH_NAME = 100;

	@Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
	
	@NotEmpty
	@Length(max = Product.MAX_LENGTH_NAME)
	@Column(nullable=false, unique=true, length = MAX_LENGTH_NAME)
	private String name;
	
	@NotEmpty
	@Length(max = Product.MAX_LENGTH_DESCRIPTION)
	@Column(length = MAX_LENGTH_DESCRIPTION)
	private String description;
	
	@Column
	@Enumerated(EnumType.STRING)
	private Color color;

	@Column
	private double price;

	@Column
	@Enumerated(EnumType.STRING)
	private Size size;
	
		

}
