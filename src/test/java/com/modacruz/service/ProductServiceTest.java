package com.modacruz.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.modacruz.Fixtures;
import com.modacruz.domain.Product;
import com.modacruz.exception.ProductAlreadyExistException;
import com.modacruz.exception.ResourceNotFoundException;
import com.modacruz.model.ProductDetails;
import com.modacruz.model.ProductForm;
import com.modacruz.repository.ProductRepository;
import com.modacruz.util.DTOUtils;

@RunWith(SpringRunner.class)
@Import(ProductService.class)
public class ProductServiceTest {
	
    private static final Long ID = 1L;
    private static final String NAME = "Product1";
    private static final String NAME_UPDATED = "updatedProduct1";
    private static final String DESCRIPTION = "description1";
    private static final String DESCRIPTION_UPDATED = "updatedDescription1";

	@MockBean
	private ProductRepository repository;
	
	@Autowired
	private ProductService service;

	
//================================================CREATE=====================================================================
	@Test
    public void testCreate_NullProductForm_ShouldThrowException(){
		assertThatThrownBy(() -> service.create(null)).isInstanceOf(IllegalArgumentException.class);
    	verifyZeroInteractions(repository);
    }

	@Test
	public void testCreate_AlreadyExistProduct_ShouldThrowException() {
    	Product product = Fixtures.createProduct(NAME, DESCRIPTION);
    	ProductForm productForm = Fixtures.createProductForm(NAME, DESCRIPTION);
    	
    	given(this.repository.findByName(anyString())).willReturn(product);
    	assertThatThrownBy(() -> service.create(productForm)).isInstanceOf(ProductAlreadyExistException.class);

        ArgumentCaptor<String> nameArgument = ArgumentCaptor.forClass(String.class);
        verify(repository, times(1)).findByName(nameArgument.capture());

        verifyNoMoreInteractions(repository);
	}


    @Test
    public void testCreate_NewProduct_ShouldSaveEntry() {
    	Product product = Fixtures.createProduct(NAME, DESCRIPTION);
    	ProductForm productForm = Fixtures.createProductForm(NAME, DESCRIPTION);
    	
    	given(this.repository.findByName(anyString())).willReturn(null);
    	given(this.repository.save(any(Product.class))).willReturn(product);
    	
    	service.create(productForm);

        ArgumentCaptor<String> nameArgument = ArgumentCaptor.forClass(String.class);
        verify(repository, times(1)).findByName(nameArgument.capture());

        ArgumentCaptor<Product> productArgument = ArgumentCaptor.forClass(Product.class);
        verify(repository, times(1)).save(productArgument.capture());
        verifyNoMoreInteractions(repository);

        Product model = productArgument.getValue();
        assertNull(model.getId());
        assertThat(model.getDescription(), is(productForm.getDescription()));
        assertThat(model.getName(), is(productForm.getName()));
    }

//================================================UPDATE=====================================================================
    
	@Test
    public void testUpdate_NullProductForm_ShouldThrowException() {
		assertThatThrownBy(() -> service.update(null,null)).isInstanceOf(IllegalArgumentException.class);
    	verifyZeroInteractions(repository);
    }

    @Test
    public void testUpdate_NotExistEntry_ShouldThrowException()  {
    	ProductForm productForm = Fixtures.createProductForm(NAME_UPDATED, DESCRIPTION_UPDATED);

        when(repository.findOne(ID)).thenReturn(null);

        assertThatThrownBy(() -> service.update(ID,productForm)).isInstanceOf(ResourceNotFoundException.class);

        verify(repository, times(1)).findOne(ID);
        verifyNoMoreInteractions(repository);
    }

    
    @Test
    public void testUpdate_ExistingEntry_ShouldUpdateEntry()  {
    	Product product = Fixtures.createProduct(ID,NAME, DESCRIPTION);
    	ProductForm productForm = Fixtures.createProductForm(NAME_UPDATED, DESCRIPTION_UPDATED);

        when(repository.findOne(anyLong())).thenReturn(product);
        
        when(repository.save(any(Product.class))).thenReturn(product);

        ProductDetails actual = service.update(ID,productForm);

        verify(repository, times(1)).findOne(anyLong());
        verify(repository, times(1)).save(any(Product.class));
        verifyNoMoreInteractions(repository);

        assertThat(product.getId(), is(ID));
        assertThat(product.getDescription(), is(productForm.getDescription()));
        assertThat(product.getName(), is(productForm.getName()));

        assertThat(actual.getId(), is(ID));
        assertThat(actual.getDescription(), is(productForm.getDescription()));
        assertThat(actual.getName(), is(productForm.getName()));
    }
    
    
//================================================DELETE=====================================================================
    
  	@Test
	public void testDelete_NullId_ShouldThrowException()  {
		assertThatThrownBy(() -> service.delete(null)).isInstanceOf(IllegalArgumentException.class);
		verifyZeroInteractions(repository);
	}

	@Test
	public void testDelete_NotExistId_ShouldThrowException() {
		when(repository.findOne(ID)).thenReturn(null);
		assertThatThrownBy(() -> service.delete(ID)).isInstanceOf(ResourceNotFoundException.class);

		verify(repository, times(1)).findOne(ID);
		verifyNoMoreInteractions(repository);
	}

      
	@Test
	public void testDelete_ExistingId_ShouldDeleteEntry()  {
		Product product = Fixtures.createProduct(ID, NAME, DESCRIPTION);

		when(repository.findOne(anyLong())).thenReturn(product);

		doNothing().when(repository).delete(anyLong());

		service.delete(ID);

		verify(repository, times(1)).findOne(anyLong());
		verify(repository, times(1)).delete(anyLong());
		verifyNoMoreInteractions(repository);
	}

    
//================================================FIND BY ID=====================================================================
	@Test
    public void testFindById_NullId_ShouldThrowException() {
		assertThatThrownBy(() -> service.findById(null)).isInstanceOf(IllegalArgumentException.class);
    	verifyZeroInteractions(repository);
    }
	
    @Test
    public void testFindById_NotExistEntry_ShouldThrowException() {
        when(repository.findOne(ID)).thenReturn(null);
        assertThatThrownBy(() -> service.findById(ID)).isInstanceOf(ResourceNotFoundException.class);

        verify(repository, times(1)).findOne(ID);
        verifyNoMoreInteractions(repository);
    }

	@Test
	public void testFindById_ExistingEntry_ShouldReturnFoundEntry(){
		Product product = Fixtures.createProduct(ID,NAME);
		ProductDetails model=DTOUtils.map(product, ProductDetails.class);

		given(this.repository.findOne(anyLong())).willReturn(product);
		ProductDetails productDetails = service.findById(ID);
		
        verify(repository, times(1)).findOne(ID);
        verifyNoMoreInteractions(repository);
        assertThat(productDetails, is(model));
	}


//================================================FIND ALL=====================================================================
    @Test
    public void testFindAll_ShouldReturnListOfEntries() {
		Product product1 = Fixtures.createProduct(1L, "Product1");
		Product product2 = Fixtures.createProduct(2L, "Product2");
		List<Product> products = Arrays.asList(product1, product2);
		List<ProductDetails> models = DTOUtils.mapList(products, ProductDetails.class);
		
		when(this.repository.findAll()).thenReturn(products);

        List<ProductDetails> actual = service.findAll();

        verify(repository, times(1)).findAll();
        verifyNoMoreInteractions(repository);

        assertThat(actual, is(models));
    }
 
 

}
