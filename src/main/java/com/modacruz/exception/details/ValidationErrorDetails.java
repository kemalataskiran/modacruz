package com.modacruz.exception.details;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidationErrorDetails {
 
    private List<FieldErrorDetails> fieldErrors = new ArrayList<>();
 
    public void addFieldError(String path, String message) {
        FieldErrorDetails error = new FieldErrorDetails(path, message);
        fieldErrors.add(error);
    }
}