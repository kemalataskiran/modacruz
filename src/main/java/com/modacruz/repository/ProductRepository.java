package com.modacruz.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.modacruz.domain.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	Product findByName(String name);

}
