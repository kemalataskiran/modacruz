package com.modacruz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.modacruz.domain.Product;
import com.modacruz.exception.ProductAlreadyExistException;
import com.modacruz.exception.ResourceNotFoundException;
import com.modacruz.model.ProductDetails;
import com.modacruz.model.ProductForm;
import com.modacruz.repository.ProductRepository;
import com.modacruz.util.DTOUtils;

@Service
@Transactional
public class ProductService {

	@Autowired
	private ProductRepository repository;

	public ProductDetails create(ProductForm form) {
		Assert.notNull(form, " @@ ProductForm is null");
		if (repository.findByName(form.getName()) != null) {
			throw new ProductAlreadyExistException(form.getName());
		}
		Product product = DTOUtils.map(form, Product.class);
		Product saved = repository.save(product);
		return DTOUtils.map(saved, ProductDetails.class);
	}

	public ProductDetails update(Long id, ProductForm form) {
		Product product = findProductById(id);
		DTOUtils.mapTo(form, product);
		Product updated = repository.save(product);
		return DTOUtils.map(updated, ProductDetails.class);
	}

	public void delete(Long id) {
		findProductById(id);
		repository.delete(id);
	}
	
	public ProductDetails findById(Long id) {
		Product product = findProductById(id);
		return DTOUtils.map(product, ProductDetails.class);
	}
	
	public List<ProductDetails> findAll() {
		List<Product> products = repository.findAll();
		return DTOUtils.mapList(products, ProductDetails.class);
	}

	private Product findProductById(Long id){
		Assert.notNull(id, "id can not be null");
		Product product = repository.findOne(id);
		if (product == null) {
			throw new ResourceNotFoundException(id);
		}
		return product;
	}

}
