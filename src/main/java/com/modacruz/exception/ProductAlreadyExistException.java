package com.modacruz.exception;

public class ProductAlreadyExistException extends RuntimeException{
	
	private static final long serialVersionUID = 4888500769389884072L;

	private String name;
	
	public ProductAlreadyExistException(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
