package com.modacruz.domain.enumaration;

public enum Size {
	XS,S,M,L,XL,XXL,S36,S37,S38,S39,S40,S41,S42,S43,S44,S45;
}
