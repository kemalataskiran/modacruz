package com.modacruz.exception;

import org.springframework.validation.BindingResult;

public class InvalidRequestException extends RuntimeException {

  private static final long serialVersionUID = -1791473765277933370L;

  private BindingResult errors;

  public InvalidRequestException(BindingResult errors) {
    this.errors = errors;
  }

  public BindingResult getErrors() {
    return errors;
  }

}
