package com.modacruz.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket buildingApi() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.apiInfo(apiInfo())
        		.select()
        		.paths(apiPaths())
                .build();
    }
    
    @SuppressWarnings("unchecked")
	private Predicate<String> apiPaths(){
    	return Predicates.or(PathSelectors.regex("/api/.*"));
    }
    
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Building API")
                .description("The online reference documentation for developers")
                .termsOfServiceUrl("http://www.feesman.com")
                .contact(new Contact("Fee super man", "http://www.feesman.com", "info@feesman.com"))
//                .license("Apache License Version 2.0")
//                .licenseUrl("https://github.com/IBM-Bluemix/news-aggregator/blob/master/LICENSE")
                .version("1.0")
                .build();
    }
}
