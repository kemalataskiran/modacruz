package com.modacruz.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.modacruz.model.ProductDetails;
import com.modacruz.model.ProductForm;
import com.modacruz.service.ProductService;
import com.modacruz.util.Constants;

@RestController
@RequestMapping(value = Constants.URI_PRODUCT)
public class ProductController {

  @Autowired
  private ProductService service;

  @PostMapping()
  public ResponseEntity<ProductDetails> create(@RequestBody @Valid ProductForm form) {
    ProductDetails productDetails = service.create(form);
    return new ResponseEntity<>(productDetails, HttpStatus.CREATED);
  }

  @PutMapping(value = "/{id}")
  public ProductDetails update(@RequestBody @Valid ProductForm form, @PathVariable("id") Long id) {
    ProductDetails updated = service.update(id, form);
    return updated;
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<Void> delete(@PathVariable("id") long id) {
    service.delete(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @GetMapping(value = "/{id}")
  @PostMapping()
  public ResponseEntity<ProductDetails> findById(@PathVariable("id") Long id) {
    ProductDetails product = service.findById(id);
    return new ResponseEntity<>(product, HttpStatus.OK);
  }

  @GetMapping()
  public ResponseEntity<List<ProductDetails>> findAll() {
    List<ProductDetails> accountActivities = service.findAll();
    return new ResponseEntity<>(accountActivities, HttpStatus.OK);
  }

}
